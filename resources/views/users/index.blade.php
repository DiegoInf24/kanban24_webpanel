@extends('layouts.main')

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ __('Users')}}</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    @if($currantWorkspace || Auth::user()->type == 'admin')
        @if($currantWorkspace && $currantWorkspace->creater->id == Auth::user()->id)
        <div class="row mb-2">
            <div class="col-sm-4">
                <button type="button" class="btn btn-danger btn-rounded mb-3" data-ajax-popup="true" data-size="lg" data-title="{{ __('Invite New User') }}" data-url="{{route('users.invite',$currantWorkspace->slug)}}">
                    <i class="mdi mdi-plus"></i> {{ __('Invite User') }}
                </button>
            </div>
        </div>
        @endif
        <div class="row">
            @foreach ($users as $user)

            <div class="col-lg-4 col-md-4 animated">
                <div class="card">
                    <div class="card-body">
                        <div class="dropdown card-widgets">
                            @if(Auth::user()->type == 'admin' && isset($user->getPlan))

                                <div class="dropdown card-widgets">
                                    <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false">
                                        <i class="dripicons-dots-3"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a href="#" onclick="confirm('Are you sure ?')?document.getElementById('delete_user_{{$user->id}}').submit():'';" class="dropdown-item"><i class="mdi mdi-trash-can mr-1"></i>{{__('Delete')}}</a>
                                        <form action="{{route('users.delete',$user->id)}}" method="post" id="delete_user_{{$user->id}}" style="display: none;">
                                            @csrf
                                            @method('DELETE')
                                        </form>
                                    </div>
                                </div>

                                <span class="badge badge-info mr-1">{{$user->getPlan->name}}</span>
                            @elseif(isset($user->is_active) && !$user->is_active)
                                <a href="#" title="{{__('Locked')}}">
                                    <i class="mdi mdi-lock-outline"></i>
                                </a>
                            @endif
                        </div>
                        <span class="float-left mr-4">
                            <img @if($user->avatar) src="{{asset('/storage/avatars/'.$user->avatar)}}" width="75px" @else avatar="{{ $user->name }}"@endif alt="" class="rounded-circle img-thumbnail">
                        </span>
                        <div class="media-body">
                            <h4 class="mt-1">{{$user->name}}</h4>
                            <p class="font-13 mb-0">{{$user->permission}}</p>
                            <p class="font-13 mb-0">{{$user->email}}</p>
                        </div>
                        <div class="clearfix"></div>
                        <ul class="row text-center list-inline">
                            @if(Auth::user()->type == 'admin')
                            <li class="col-6 mt-2">
                                <h5 class="mb-1">{{$user->countWorkspace()}}</h5>
                                <p class="mb-0 font-13">{{ __('Number of Workspaces')}}</p>
                            </li>
                            <li class="col-6 mt-2">
                                <h5 class="mb-1">{{$user->countUsers(($currantWorkspace)?$currantWorkspace->id:'')}}</h5>
                                <p class="mb-0 font-13">{{ __('Number of Users')}}</p>
                            </li>
                            <li class="col-6 mt-2">
                                <h5 class="mb-1">{{$user->countClients(($currantWorkspace)?$currantWorkspace->id:'')}}</h5>
                                <p class="mb-0 font-13">{{ __('Number of Clients')}}</p>
                            </li>
                            @endif
                            <li class="col-6 mt-2">
                                <h5 class="mb-1">{{$user->countProject(($currantWorkspace)?$currantWorkspace->id:'')}}</h5>
                                <p class="mb-0 font-13">{{ __('Number of Projects')}}</p>
                            </li>
                            @if(Auth::user()->type != 'admin')
                            <li class="col-6 mt-2">
                                <h5 class="mb-1">{{$user->countTask(($currantWorkspace)?$currantWorkspace->id:'')}}</h5>
                                <p class="mb-0 font-13">{{ __('Number of Tasks')}}</p>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    @else
        <div class="row justify-content-center animated">
            <div class="col-lg-4">
                <div class="text-center">
                    <img src="{{ asset('images/file-searching.svg') }}" height="90" alt="File not found Image">

                    <h1 class="text-error mt-4">404</h1>
                    <h4 class="text-uppercase text-danger mt-3">{{ __('Page Not Found') }}</h4>
                    <p class="text-muted mt-3">{{ __('It\'s looking like you may have taken a wrong turn. Don\'t worry... it happens to the best of us. Here\'s a little tip that might help you get back on track.')}}</p>
                        <a class="btn btn-info mt-3" href="{{route('home')}}"><i class="mdi mdi-reply"></i> {{ __('Return Home')}}</a>
                </div> <!-- end /.text-center-->
            </div> <!-- end col-->
        </div>
    @endif
</div>
<!-- container -->
@endsection