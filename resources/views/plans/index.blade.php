@extends('layouts.main')

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="mb-0">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ __('Plans')}}</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    @if(Auth::user()->type == 'admin')
        @if(empty(env('STRIPE_KEY')) || empty(env('STRIPE_SECRET')))
            <div class="alert alert-warning"><i class="dripicons-warning"></i> {{__('Please set stripe api key & secret key for add new plan')}}</div>
        @else
        <div class="row mb-2">
            <div class="col-sm-4">
                <button type="button" class="btn btn-danger btn-rounded mb-3" data-ajax-popup="true" data-size="lg" data-title="{{ __('Add Plan') }}" data-url="{{route('plans.create')}}">
                    <i class="mdi mdi-plus"></i> {{ __('Add Plan') }}
                </button>
            </div>
        </div>
        @endif
    @endif

        <div class="row justify-content-center">
            <div class="col-xl-10">
            @if(Auth::user()->type != 'admin')
                <!-- Pricing Title-->
                <div class="text-center">
                    <h3 class="mb-2">{{__('Our Plans and Pricing')}}</h3>
                    <p class="text-muted w-50 m-auto">
                        {{__('We have plans and prices that fit your business perfectly. Make your client site a success with our products.')}}
                    </p>
                </div>
                <!-- Plans -->
            @endif
                <div class="row mt-5 mb-1">
                    @foreach ($plans as $key => $plan)
                    <div class="col-md-4">
                        <div class="card card-pricing card-pricing-recommended">
                            <div class="card-body text-center">
                                @if(Auth::user()->type == 'admin')
                                    <div class="dropdown card-widgets">
                                        <a href="#" class="dropdown-toggle arrow-none" data-toggle="dropdown" aria-expanded="false">
                                            <i class="dripicons-dots-3"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item" data-ajax-popup="true" data-size="lg" data-title="Edit Plan" data-url="{{route('plans.edit',$plan->id)}}"><i class="mdi mdi-pencil mr-1"></i>{{__('Edit')}}</a>
                                        </div>
                                    </div>
                                @endif
                                @if(Auth::user()->plan == $plan->id)<div class="card-pricing-plan-tag">{{__('Currant Plan')}}</div>@endif
                                <p class="card-pricing-plan-name font-weight-bold text-uppercase">{{ $plan->name }}</p>
                                <img @if($plan->image) src="{{asset('/storage/plans/'.$plan->image)}}" @else avatar="{{ $plan->name }}" @endif alt="plan image" class="rounded-circle card-pricing-icon">
                                <h2 class="card-pricing-price">${{ $plan->price }} <span>@if($plan->duration!='Unlimited')/@endif {{ __($plan->duration) }}</span></h2>
                                <ul class="card-pricing-features">
                                    <li>{{ ($plan->max_workspaces < 0)?__('Unlimited'):$plan->max_workspaces }} {{__('Workspaces')}}</li>
                                    <li>{{ ($plan->max_users<0)?__('Unlimited'):$plan->max_users }} {{__('Users Per Workspace')}}</li>
                                    <li>{{ ($plan->max_clients<0)?__('Unlimited'):$plan->max_clients }} {{__('Clients Per Workspace')}}</li>
                                    <li>{{ ($plan->max_projects<0)?__('Unlimited'):$plan->max_projects }} {{__('Projects Per Workspace')}}</li>
                                </ul>
                                    @if($plan->description)
                                    <p>
                                        {{$plan->description}}
                                    </p>
                                    @endif
                                @if(Auth::user()->type != 'admin')
                                    @if(Auth::user()->plan != $plan->id)
                                        <a href="{{route('stripe',\Illuminate\Support\Facades\Crypt::encrypt($plan->id))}}" class="btn btn-primary mt-4 mb-2 btn-rounded text-white">{{__('Choose Plan')}}</a>
                                    @endif
                                @endif
                            </div>
                        </div> <!-- end Pricing_card -->
                    </div> <!-- end col -->
                        @if(($key+1)%3 == 0)
                            </div>
                            <div class="row mt-4 mb-1">
                        @endif
                    @endforeach
                </div>
                <!-- end row -->

            </div> <!-- end col-->
        </div>

</div>
<!-- container -->
@endsection