@extends('layouts.main')

@section('content')
<!-- Start Content-->
<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title">{{ __('Orders')}}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <table id="selection-datatable" class="table dt-responsive nowrap text-capitalize" width="100%">
                        <thead>
                            <tr>
                                <th>{{__('Order Id')}}</th>
                                <th>{{__('Name')}}</th>
                                <th>{{__('Plan Name')}}</th>
                                <th>{{__('Price')}}</th>
                                <th>{{__('Status')}}</th>
                                <th>{{__('Date')}}</th>
                                <th>{{__('Invoice')}}</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($orders as $order)
                            <tr>
                                <td>{{$order->order_id}}</td>
                                <td>{{$order->user_name}}</td>
                                <td>{{$order->plan_name}}</td>
                                <td>${{number_format($order->price)}}</td>
                                <td>
                                    @if($order->payment_status == 'succeeded')
                                        <i class="mdi mdi-circle text-success"></i> {{$order->payment_status}}
                                    @else
                                        <i class="mdi mdi-circle text-danger"></i> {{$order->payment_status}}
                                    @endif
                                </td>
                                <td>{{$order->created_at->format('d M Y')}}</td>
                                <td>
                                    @if(!empty($order->receipt))
                                        <a href="{{$order->receipt}}" target="_blank" class="btn btn-outline-primary btn-rounded btn-sm"><i class="mdi mdi-printer mr-1"></i> {{__('Invoice')}}</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>

</div>

<!-- container -->
@endsection

@push('style')
    <link href="{{asset('css/vendor/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/vendor/responsive.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/vendor/buttons.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/vendor/select.bootstrap4.css')}}" rel="stylesheet" type="text/css" />
@endpush
@push('scripts')
    <script src="{{asset('js/vendor/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/vendor/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('js/vendor/dataTables.responsive.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $("#selection-datatable").DataTable({
                order: [],
                select: {style: "multi"},
                language: {paginate: {previous: "<i class='mdi mdi-chevron-left'>", next: "<i class='mdi mdi-chevron-right'>"}},
                drawCallback: function () {
                    $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
                }
            });
        });
    </script>
@endpush