<?php

namespace App\Http\Controllers;

use App\ClientWorkspace;
use App\Order;
use App\Plan;
use App\User;
use App\Utility;
use App\UserProject;
use App\Task;
use App\Todo;
use App\Calendar;
use App\UserWorkspace;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Storage;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function check()
    {
        $user = Auth::user();
        if($user->type != 'admin') {
            $plan = Plan::find($user->plan);
            if ($plan) {
                if ($plan->duration != 'Unlimited') {
                    $datetime1 = new \DateTime($user->plan_expire_date);
                    $datetime2 = new \DateTime(date('Y-m-d'));
                    $interval = $datetime1->diff($datetime2);
                    $days = $interval->format('%a');
                    if ($days <= 0) {
                        $user->assignPlan(1);
                        return redirect()->route('home')->with('info', __('Your Plan is expired.'));
                    }
                }
            } else {
                return redirect()->route('home')
                    ->with('error', __('Plan not found'));
            }
        }
        return redirect()->route('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($slug = '')
    {
        $userObj = Auth::user();
        $currantWorkspace = Utility::getWorkspaceBySlug($slug);
        if($userObj->type=='admin'){
            $totalUsers = User::where('type','!=','admin')->count();
            $totalPaidUsers = User::where('type','!=','admin')->where('plan','!=',1)->count();
            $totalOrderAmount = Order::where('payment_status','=','succeeded')->sum('price');
            $totalOrders = Order::where('payment_status','=','succeeded')->count();
            $totalPlans = Plan::count();
            $mostPlans = Plan::where('id',function($query){
                $query->select('plan_id')
                    ->from('orders')
                    ->groupBy('plan_id')
                    ->orderBy(\DB::raw('COUNT(plan_id)'))->limit(1);
            })->first();

            $chartData = $this->getOrderChart(['duration'=>'week']);
            return view('home', compact('totalUsers','totalPaidUsers','totalOrders','totalOrderAmount','totalPlans','mostPlans','chartData'));
        }
        elseif($currantWorkspace) {

            $totalProject = UserProject::join("projects","projects.id","=","user_projects.project_id")->where("user_id","=",$userObj->id)->where('projects.workspace','=',$currantWorkspace->id)->count();
            $totalClients = ClientWorkspace::where("workspace_id","=",$currantWorkspace->id)->count();
            if($currantWorkspace->permission == 'Owner') {
                $totalTask = UserProject::join("tasks", "tasks.project_id", "=", "user_projects.project_id")->join("projects", "projects.id", "=", "user_projects.project_id")->where("user_id", "=", $userObj->id)->where('projects.workspace', '=', $currantWorkspace->id)->count();
                $completeTask = UserProject::join("tasks", "tasks.project_id", "=", "user_projects.project_id")->join("projects", "projects.id", "=", "user_projects.project_id")->where("user_id", "=", $userObj->id)->where('projects.workspace', '=', $currantWorkspace->id)->where('tasks.status', '=', 'done')->count();
                $tasks = Task::select('tasks.*')->join("user_projects", "tasks.project_id", "=", "user_projects.project_id")->join("projects", "projects.id", "=", "user_projects.project_id")->where("user_id", "=", $userObj->id)->where('projects.workspace', '=', $currantWorkspace->id)->orderBy('tasks.id','desc')->limit(4)->get();
            }
            else{
                $totalTask = UserProject::join("tasks", "tasks.project_id", "=", "user_projects.project_id")->join("projects", "projects.id", "=", "user_projects.project_id")->where("user_id", "=", $userObj->id)->where('projects.workspace', '=', $currantWorkspace->id)->where('tasks.assign_to', '=', $userObj->id)->count();
                $completeTask = UserProject::join("tasks", "tasks.project_id", "=", "user_projects.project_id")->join("projects", "projects.id", "=", "user_projects.project_id")->where("user_id", "=", $userObj->id)->where('projects.workspace', '=', $currantWorkspace->id)->where('tasks.assign_to', '=', $userObj->id)->where('tasks.status', '=', 'done')->count();
                $tasks = Task::select('tasks.*')->join("user_projects", "tasks.project_id", "=", "user_projects.project_id")->join("projects", "projects.id", "=", "user_projects.project_id")->where("user_id", "=", $userObj->id)->where('projects.workspace', '=', $currantWorkspace->id)->where('tasks.assign_to', '=', $userObj->id)->orderBy('tasks.id','desc')->limit(4)->get();
            }


            $totalMembers = UserWorkspace::where('workspace_id','=',$currantWorkspace->id)->count();

            $projectProcess = UserProject::join("projects","projects.id","=","user_projects.project_id")->where("user_id","=",$userObj->id)->where('projects.workspace','=',$currantWorkspace->id)->groupBy('projects.status')->selectRaw('count(projects.id) as count, projects.status')->pluck('count','projects.status');
            $arrProcessLable= [];
            $arrProcessPer=[];
            $arrProcessLable = [];
            foreach ($projectProcess as $lable => $process){
                $arrProcessLable[]=$lable;
                $arrProcessPer[] = round(($process*100)/$totalProject,2);
            }
            $arrProcessClass = ['text-success','text-primary','text-danger'];

            $todos = Todo::where("created_by", "=", $userObj->id)->where('workspace','=',$currantWorkspace->id)->orderBy('id','desc')->limit(5)->get();


            $startDate = date('Y-m-d 00:00:00');
            $endDate = date('Y-m-d H:i:s',strtotime($startDate."+1 day"));

            $calendars = Calendar::where('workspace','=',$currantWorkspace->id)->where('created_by','=',$userObj->id)->where(function ($q) use($startDate,$endDate){
                $q->where('start',">=",$startDate)->where('end',"<=",$endDate)->whereNotNull('end');
            })->orWhere(function ($q) use($startDate,$endDate){
                $q->where('start',">=",$startDate)->where('start',"<=",$endDate)->whereNull('end');
            })->orderBy('id','desc')->get();

            $chartData = app('App\Http\Controllers\ProjectController')->getProjectChart(['workspace_id'=>$currantWorkspace->id,'duration'=>'week']);

            return view('home', compact('currantWorkspace','totalProject','totalClients','totalTask','totalMembers','arrProcessLable','arrProcessPer','arrProcessClass','completeTask','tasks','todos','calendars','chartData'));
        }
        else{
            return view('home', compact('currantWorkspace'));
        }
    }
    public function getOrderChart($arrParam){
        $arrDuration = [];
        if($arrParam['duration']){

            if($arrParam['duration'] == 'week'){
                $previous_week = strtotime("-1 week +1 day");


                for ($i=0;$i<7;$i++){
                    $arrDuration[date('Y-m-d',$previous_week)] = date('D',$previous_week);
                    $previous_week = strtotime(date('Y-m-d',$previous_week). " +1 day");
                }
            }
        }
//        dd($arrDuration);
        $arrTask = [];
        $arrTask['label'] = [];
        $arrTask['data'] = [];
        foreach ($arrDuration as $date => $label){


            $data = Order::select(\DB::raw('count(*) as total'))
                ->whereDate('created_at','=',$date)->first();
            $arrTask['label'][]=$label;
            $arrTask['data'][]=$data->total;
        }
        return $arrTask;
    }
}
